from visitor import Visitor
from components import Source, Sink, Sanitizer
from infos import SourceInfo, SanitizerInfo, SinkInfo, Info
from names import Names


class Analyser(Visitor):
    class Vulnerable:
        def __init__(self, source=None, sink=None, sanitizer=None):
            self.source = source
            self.sink = sink
            self.sanitizer = sanitizer

        def __hash__(self):
            args = self.source.path if self.source else ""
            args += self.sink.path if self.sink else ""
            args += str(self.sanitizer) if self.sanitizer else ""
            return hash(args)

        def __eq__(self, other):
            return self.source == other.source and self.sink == other.sink \
                and self.sanitizer == self.sanitizer

        def __repr__(self):
            return "\"source\":[\"" + str(self.source) + "\"],\n\"sink\":[\"" + \
                str(self.sink) + "\"],\n\"sanitizer\":[\"" + str(self.sanitizer) + \
                "\"]"

    def __init__(self):
        self.tests   = []
        self.context = [{}]
        self.vulns   = set()

    def add_test(self, test: Info):
        """
        Add new test to context
        """
        self.tests.append(test)

    def new_full_context(self, test: Info):
        """
        Create new test and variable contexts (e.g. if and while)
        """
        self.tests.append(test)
        self.context.append({})

    def new_context(self):
        """
        Create a new variable context (e.g. blocks)
        """
        self.context.append({})

    def pop_test(self) -> Info:
        """
        Remove a test context
        """
        return self.tests.pop()

    def push_context(self, ctx: {str: [Info]}):
        """
        Push a new context to the list
        """
        self.context.append(ctx)

    def pop_context(self) -> {str: [Info]}:
        """
        Pop the current variable context
        """
        return self.context.pop()

    @staticmethod
    def subtract_contexts(c1: {str: [Info]}, c2: {str: [Info]}) -> {str: [Info]}:
        subtraction1 = c1.keys() - c2.keys()
        subtraction2 = c2.keys() - c1.keys()
        res = {}
        for key in subtraction1:
            res[key] = c1[key] if key in c1 else c2[key]
        for key in subtraction2:
            res[key] = c1[key] if key in c1 else c2[key]
        return res

    @staticmethod
    def intersect_contexts(c1: {str: [Info]}, c2: {str: [Info]}) -> {str: [Info]}:
        intersect = c1.keys() & c2.keys()
        res = {}
        for key in intersect:
            res[key] = list(set(c1[key] + c2[key]))
        return res

    @staticmethod
    def join_contexts(c1: {str: [Info]}, c2: {str: [Info]}) -> {str: [Info]}:
        """
        Join 2 contexts
        """
        for var in c1:
            if var not in c2:
                c2[var] = c1[var]
            else:
                c2[var].extend(c1[var])

        return c2

    def join_context(self, other: {str: [Info]}):
        """
        Join given context with the current
        """
        for var in other:
            if var not in self.context[-1]:
                older = self.get_var(var)
                if len(older) == 1 and older[0] == Info():
                    self.context[-1][var] = other[var]
                else:
                    self.context[-1][var] = older + other[var]
            else:
                self.context[-1][var].extend(other[var])

    def replace_context(self, other: {str, Info}):
        """
        Replace the variables common to both contexts (given context overrides current)
        """
        for var in other:
            self.context[-1][var] = other[var]

    def join_tests(self) -> Info:
        """
        Join all test contexts
        """
        res = Info()
        for info in self.tests:
            res |= info

        return res

    def equal_contexts(self, other: {str: [Info]}) -> bool:
        """
        Compare given context with current
        """
        if len(self.context[-1]) != len(other):
            return False

        for var in other:
            if var not in self.context[-1] or \
               self.context[-1][var] != other[var]:
                return False
        return True

    def get_var(self, var: str) -> [Info]:
        """
        Get current variable Info
        """
        for context in reversed(self.context):
            if var in context.keys():
                return context[var]
        return self.context[0][var] if var in self.context[0] else [Info()]

    def add_var(self, var: str, info: [Info], join: bool):
        """
        Add variable to current context
        """
        if len(self.tests) != 0:
            if var in self.context[-1] and join:
                self.context[-1][var].extend([x | self.join_tests() for x in info])
            else:
                self.context[-1][var] = [x | self.join_tests() for x in info]
        else:
            if var in self.context[0] and join:
                self.context[0][var].extend(info)
            else:
                self.context[0][var] = info

    def addSource(self, source: Source):
        """
        Add SourceInfo to given environment
        """
        path_split = source.path.split('.', 1)
        source_info = SourceInfo(path_split[1] if len(path_split) > 1
                                 else "", source)
        info = Info.source(source_info)
        if path_split[0] not in self.context[0]:
            self.context[0][path_split[0]] = [info]
        else:
            self.context[0][path_split[0]] = [x | info for x in self.context[0][path_split[0]]]

    def addSink(self, sink: Sink):
        """
        Add Sink to given environment
        """
        path_split = sink.path.split('.')
        sink_info = SinkInfo(path_split[1] if len(path_split) > 1
                             else "", sink)
        info = Info.sink(sink_info)
        if path_split[0] not in self.context[0]:
            self.context[0][path_split[0]] = [info]
        else:
            self.context[0][path_split[0]] = [x | info for x in self.context[0][path_split[0]]]

    def addSanitizer(self, sanitizer: Sanitizer):
        """
        Add Sanitizer to given environment
        """
        path_split = sanitizer.path.split('.')
        sanitizer_info = SanitizerInfo(path_split[1] if len(path_split) > 1
                                       else "", sanitizer)
        info = Info.sanitizer(sanitizer_info)
        if path_split[0] not in self.context[0]:
            self.context[0][path_split[0]] = [info]
        else:
            self.context[0][path_split[0]] = [x | info for x in self.context[0][path_split[0]]]

    def visit_assignment_expression(self, node):
        left  = node.left.accept(self)

        right = node.right.accept(self)
        all_ctx = self.join_tests()

        # need to add the context to the right side
        for i in range(0, len(right)):
            right[i] |= all_ctx

        sinks = []
        for info in left:
            sinks.extend(info.get_sinks())

        vuln = False
        for info in right:
            sources    = info.get_sources()
            sanitizers = [s.sanitizer for s in info.get_sanitizers()]

            # sink = source()
            if len(sinks) != 0 and len(sources) != 0:
                for source in sources:
                    for sink in sinks:
                        self.vulns.add(Analyser.Vulnerable(source.source, sink.sink, sanitizers))
                vuln = True

        if vuln:
            return left

        # Get name of left side
        left_split = node.left.accept(Names()).split(".", 1)
        if len(left_split) != 2:
            left_split = (left_split[0], "")

        res = Info()

        # left = source()
        sources = []
        for info in right:
            sources.extend(info.source_infos)
        if len(sources) != 0:
            for source in sources:
                path = ""
                if left_split[1] == "":
                    path = source.path
                elif source.path == "":
                    path = left_split[1]
                else:
                    path = left_split[1] + "." + source.path
                res |= Info.source(SourceInfo(path, source.source))

        # left = sink()
        sinks = []
        for info in right:
            sinks.extend(info.sink_infos)
        if len(sinks) != 0:
            for sink in sinks:
                path = ""
                if left_split[1] == "":
                    path = sink.path
                elif sink.path == "":
                    path = left_split[1]
                else:
                    path = left_split[1] + "." + sink.path
                res |= Info.sink(SinkInfo(path, sink.sink))

        # left = sanitizer()
        sanitizers = []
        for info in right:
            sanitizers.extend(info.sanitizer_infos)
        if len(sanitizers) != 0:
            for sanitizer in sanitizers:
                path = ""
                if left_split[1] == "":
                    path = sanitizer.path
                elif sanitizer.path == "":
                    path = left_split[1]
                else:
                    path = left_split[1] + "." + sanitizer.path
                res |= Info.sanitizer(SanitizerInfo(path, sanitizer.sanitizer))

        if node.operator not in ["=", "*=", "**=", "/=", "%=",
                                 "+=", "-=", "<<=", ">>=", ">>>="]:
            raise ValueError("Unknown operator")

        self.add_var(left_split[0], [res], node.operator != "=")

        return self.get_var(left_split[0])

    def visit_binary_expression(self, node):
        left  = node.left.accept(self)
        right = node.right.accept(self)

        if node.operator not in ["instanceof", "in", "+", "-", "*", "/", "%",
                                 "**", "|", "^", "&", "==", "!=", "===", "!==",
                                 "<", ">", "<=", ">=", "<<", ">>", ">>>"]:
            raise ValueError("Unknown operator")

        return left + right

    def visit_block_statement(self, node):
        for statement in node.body:
            statement.accept(self)

    def visit_call_expression(self, node):
        callee_info = node.callee.accept(self)
        all_ctx = self.join_tests()
        for i in range(len(callee_info)):  # add the context
            callee_info[i] |= all_ctx


        sinks = []
        for info in callee_info:
            sinks.extend(info.get_sinks())

        is_sink = len(sinks) != 0

        res = callee_info.copy()
        if len(node.arguments) == 0 and not all_ctx.is_empty():  # special case for implicit loops
            for info in callee_info:
                sources = info.get_sources()
                sanitizers = info.get_sanitizers()
                for source in sources:
                    for sink in sinks:
                        self.vulns.add(Analyser.\
                                       Vulnerable(source.source, sink.sink,
                                                  [s.sanitizer for s in sanitizers]))

        for arg in node.arguments:
            arg_info = arg.accept(self)
            for i in range(len(arg_info)):
                arg_info[i] |= all_ctx

            for info in arg_info:
                sources = info.get_sources()
                res.extend(callee_info.copy())
                for x in sources:
                    for i in range(len(res) - len(callee_info), len(res)):
                        res[i] |= Info.source(x)

                # Check if source/sink flow
                if len(sources) != 0:
                    sanitizers = info.get_sanitizers()
                    for x in sanitizers:
                        for i in range(len(res) - len(callee_info), len(res)):
                            res[i] |= Info.sanitizer(x)
                    if is_sink:
                        # Add all possible source/sink pairs
                        for source in sources:
                            for sink in sinks:
                                self.vulns.add(Analyser.\
                                               Vulnerable(source.source, sink.sink,
                                                          [s.sanitizer for s in sanitizers]))
        return list(set(res))

    def visit_expression_statement(self, node):
        node.expression.accept(self)

    def visit_identifier(self, node):
        return self.get_var(node.name)

    def visit_if_statement(self, node):
        test_info = node.test.accept(self)

        test = Info()
        for info in test_info:
            # keep track of both source and sanitizers of the test
            sources = info.get_sources()
            for source in sources:
                test |= Info.source(source)
            if len(sources) != 0:  # only keep track of sanitizers when there is sources
                for sanitizer in info.get_sanitizers():
                    test |= Info.sanitizer(sanitizer)

        self.new_full_context(test)
        node.consequent.accept(self)
        prev = self.pop_context()

        if node.alternate:
            self.new_context()
            node.alternate.accept(self)
            current_context = self.pop_context()
            intersection = Analyser.intersect_contexts(prev, current_context)
            self.replace_context(intersection)
            subtraction = Analyser.subtract_contexts(prev, current_context)
            cont = Analyser.join_contexts(self.context[-1], subtraction)
            self.replace_context(cont)
        else:
            self.join_context(prev)

    def visit_literal(self, node):
        return [Info()]

    def visit_member_expression(self, node):
        obj_info = node.obj.accept(self)
        prop     = node.prop.name

        return obj_info[0].proceed(prop)

    def visit_spread_element(self, node):
        return node.argument.accept(self)

    def visit_while_statement(self, node):
        def visit_test(test) -> [Info]:
            test_info = test.accept(self)

            test = Info()
            for info in test_info:
                # keep track of both source and sanitizers of the test
                sources = info.get_sources()
                for source in sources:
                    test |= Info.source(source)
                if len(sources) != 0:  # only keep track of sanitizers when there is sources
                    for sanitizer in info.get_sanitizers():
                        test |= Info.sanitizer(sanitizer)

            return test

        self.new_full_context(visit_test(node.test))

        node.body.accept(self)

        # Check if there was any assignment
        changed = len(self.context[-1]) != 0

        i = 5
        while changed and i != 0:
            prev = self.pop_context()
            self.join_context(prev.copy())
            self.pop_test()
            self.add_test(visit_test(node.test))
            self.push_context(prev.copy())
            node.body.accept(self)

            changed = not self.equal_contexts(prev)
            i -= 1


        self.pop_test()
        self.join_context(self.pop_context())
