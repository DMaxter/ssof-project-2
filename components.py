class Source:
    def __init__(self, path):
        self.path = path

    def __repr__(self):
        return "Source(" + self.path + ")"

class Sanitizer:
    def __init__(self, path):
        self.path = path

    def __repr__(self):
        return "Sanitizer(" + self.path + ")"

class Sink:
    def __init__(self, path):
        self.path = path

    def __repr__(self):
        return "Sink(" + self.path + ")"
