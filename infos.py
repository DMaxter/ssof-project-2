from components import Source, Sanitizer, Sink


class SourceInfo:
    def __init__(self, path: str, source: Source):
        self.path = path
        self.source = source

    def __copy__(self):
        return SourceInfo(self.path, self.source)

    def __eq__(self, other):
        return self.path == other.path and self.source == other.source

    def __hash__(self):
        return hash(self.path + self.source.path)

    def final(self):
        return len(self.path) == 0

    def __repr__(self):
        return "SourceInfo(" + self.path + ", " + str(self.source) + ")"

class SanitizerInfo:
    def __init__(self, path: str, sanitizer: Sanitizer):
        self.path = path
        self.sanitizer = sanitizer

    def __copy__(self):
        return SanitizerInfo(self.path, self.sanitizer)

    def __eq__(self, other):
        return self.path == other.path and self.sanitizer == other.sanitizer

    def __hash__(self):
        return hash(self.path + self.sanitizer.path)

    def final(self):
        return len(self.path) == 0

    def __repr__(self):
        return "SanitizerInfo(" + self.path + ", " + str(self.sanitizer) + ")"

class SinkInfo:
    def __init__(self, path: str, sink: Sink):
        self.path = path
        self.sink = sink

    def __copy__(self):
        return SinkInfo(self.path, self.sink)

    def __eq__(self, other):
        return self.path == other.path and self.sink == other.sink

    def __hash__(self):
        return hash(self.path + self.sink.path)

    def final(self):
        return len(self.path) == 0

    def __repr__(self):
        return "SinkInfo(" + self.path + ", " + str(self.sink) + ")"

class Info:
    def __init__(self):
        self.source_infos = set()
        self.sanitizer_infos = set()
        self.sink_infos = set()

    @staticmethod
    def source(source_info: SourceInfo):
        res = Info()
        res.source_infos.add(source_info)
        return res

    @staticmethod
    def sanitizer(sanitizer_info: SanitizerInfo):
        res = Info()
        res.sanitizer_infos.add(sanitizer_info)
        return res

    @staticmethod
    def sink(sink_info: SinkInfo):
        res = Info()
        res.sink_infos.add(sink_info)
        return res

    def is_empty(self):
        return len(self.source_infos) == 0 and len(self.sanitizer_infos) == 0 and len(self.sink_infos) == 0

    def proceed(self, name: str) -> []:
        new_info = Info()
        for source_info in self.source_infos:
            path_split = source_info.path.split('.', 1)
            if path_split[0] == name:
                new_info.source_infos.add(SourceInfo(path_split[1] if len(path_split) > 1 else "", source_info.source))
            elif path_split[0] == "":
                new_info.source_infos.add(source_info)
        for sanitizer_info in self.sanitizer_infos:
            path_split = sanitizer_info.path.split('.', 1)
            if path_split[0] == name:
                new_info.sanitizer_infos.add(SanitizerInfo(path_split[1] if len(path_split) > 1 else "", sanitizer_info.sanitizer))
        for sink_info in self.sink_infos:
            path_split = sink_info.path.split('.', 1)
            if path_split[0] == name:
                new_info.sink_infos.add(SinkInfo(path_split[1] if len(path_split) > 1 else "", sink_info.sink))
        return [new_info]

    def get_sources(self):
        res = []
        for s in self.source_infos:
            if s.final():
                res.append(s)
        return res

    def get_sanitizers(self):
        res = []
        for s in self.sanitizer_infos:
            if s.final():
                res.append(s)
        return res

    def get_sinks(self):
        res = []
        for s in self.sink_infos:
            if s.final():
                res.append(s)
        return res

    def __or__(self, other):
        res = Info()
        res.source_infos = self.source_infos | other.source_infos
        res.sanitizer_infos = self.sanitizer_infos | other.sanitizer_infos
        res.sink_infos = self.sink_infos | other.sink_infos
        return res

    def __eq__(self, other):
        source_infos = self.source_infos == other.source_infos
        sanitizer_infos = self.sanitizer_infos == other.sanitizer_infos
        sink_infos = self.sink_infos == other.sink_infos
        return source_infos and sanitizer_infos and sink_infos

    def __hash__(self):
        return hash((frozenset(self.source_infos), frozenset(self.sink_infos), frozenset(self.sanitizer_infos)))

    def __repr__(self):
        return "Info(" + str(self.source_infos) + ", " + str(self.sanitizer_infos) + "," + str(self.sink_infos) + ")"
