from visitor import Visitor

class Names(Visitor):
    def visit_assignment_expression(self, node):
        pass

    def visit_binary_expression(self, node):
        pass

    def visit_block_statement(self, node):
        pass

    def visit_call_expression(self, node):
        return node.callee.accept(self)

    def visit_expression_statement(self, node):
        pass

    def visit_identifier(self, node):
        return node.name

    def visit_if_statement(self, node):
        pass

    def visit_literal(self, node):
        return node.raw

    def visit_member_expression(self, node):
        return node.obj.accept(self) + "." + node.prop.accept(self)

    def visit_spread_element(self, node):
        pass

    def visit_while_statement(self, node):
        pass
