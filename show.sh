#!/bin/bash

TEST_PATH=./proj-slices

# To show results for all tests in the shared repo change 1 to 2
for i in $(find $TEST_PATH -mindepth 1 -type d -not -path "$TEST_PATH/.git/*" | sort )
do
	echo $i
    pr -m -w 120 -S"| " -t $i/script.js $i/program.output.json $i/patterns.json
    read -p ""
done
