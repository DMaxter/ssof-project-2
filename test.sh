#!/bin/bash

TEST_PATH=./proj-slices
PROGRAM="python tool.py"

# To run all tests in the shared repo, change 1 to 2
for i in $(find $TEST_PATH -mindepth 1 -type d -not -path "$TEST_PATH/.git/*" | sort )
do
	echo $i
    if ! $PROGRAM $i/program.json $i/patterns.json 2> /dev/null ; then
        echo "Failed $i"
    fi
done
