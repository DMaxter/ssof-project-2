from typing import Pattern

import abc
import json
import sys

from analyser import Analyser
from components import Source, Sanitizer, Sink
from visitor import Visitor

program = sys.argv[1]
patterns = sys.argv[2]

# AST definition
class Node(abc.ABC):
    @abc.abstractmethod
    def accept(self, visitor: Visitor):
        pass

class Expression(Node):
    pass

class Statement(Node):
    pass

class AssignmentExpression(Expression):
    def __init__(self, operator: str, left: Expression, right: Expression):
        self.operator = operator
        self.left     = left
        self.right    = right

    def accept(self, visitor: Visitor):
        return visitor.visit_assignment_expression(self)

class BinaryExpression(Expression):
    def __init__(self, operator: str, left: Expression, right: Expression):
        self.operator = operator
        self.left     = left
        self.right    = right

    def accept(self, visitor: Visitor):
        return visitor.visit_binary_expression(self)

class BlockStatement(Statement):
    def __init__(self, body: [Statement]):
        self.body = body

    def accept(self, visitor: Visitor):
        return visitor.visit_block_statement(self)

class SpreadElement(Expression):
    def __init__(self, argument: Expression):
        self.argument = argument

    def accept(self, visitor: Visitor):
        return visitor.visit_spread_element(self)

class CallExpression(Expression):
    def __init__(self, callee: Expression, arguments: [Expression]):
        self.callee = callee
        self.arguments = arguments

    def accept(self, visitor: Visitor):
        return visitor.visit_call_expression(self)

class ExpressionStatement(Statement):
    def __init__(self, expression: Expression, directive: str = None):
        self.expression = expression
        self.directive  = directive

    def accept(self, visitor: Visitor):
        return visitor.visit_expression_statement(self)

class Identifier(Expression):
    def __init__(self, name: str):
        self.name = name

    def accept(self, visitor: Visitor):
        return visitor.visit_identifier(self)

class IfStatement(Statement):
    def __init__(self, test: Expression, consequent: Statement, alternate: Statement = None):
        self.test       = test
        self.consequent = consequent
        self.alternate  = alternate

    def accept(self, visitor: Visitor):
        return visitor.visit_if_statement(self)

class Literal(Expression):
    def __init__(self, value, raw, regex: Pattern = None):
        self.value = value
        self.raw   = raw
        self.regex = regex

    def accept(self, visitor: Visitor):
        return visitor.visit_literal(self)

class MemberExpression(Expression):
    def __init__(self, computed: bool, obj: Expression, prop: Expression):
        self.computed = computed
        self.obj      = obj
        self.prop     = prop

    def accept(self, visitor: Visitor):
        return visitor.visit_member_expression(self)

class WhileStatement(Statement):
    def __init__(self, test: Expression, body: Statement):
        self.test = test
        self.body = body

    def accept(self, visitor: Visitor):
        return visitor.visit_while_statement(self)

# Parsing functions
def parse_elem(elem: dict) -> Node:
    if elem["type"] == "AssignmentExpression":
        return AssignmentExpression(elem["operator"], parse_elem(elem["left"]), \
                                    parse_elem(elem["right"]))
    if elem["type"] == "BinaryExpression":
        return BinaryExpression(elem["operator"], parse_elem(elem["left"]), \
                                parse_elem(elem["right"]))
    if elem["type"] == "BlockStatement":
        return BlockStatement([parse_elem(x) for x in elem["body"]])
    if elem["type"] == "CallExpression":
        return CallExpression(parse_elem(elem["callee"]), \
                              [parse_elem(x) for x in elem["arguments"]])
    if elem["type"] == "ExpressionStatement":
        return ExpressionStatement(parse_elem(elem["expression"]), \
                                   elem["directive"] if "directive" in elem else None)
    if elem["type"] == "Identifier":
        return Identifier(elem["name"])
    if elem["type"] == "IfStatement":
        return IfStatement(parse_elem(elem["test"]), parse_elem(elem["consequent"]), \
                           parse_elem(elem["alternate"]) if elem["alternate"] else None)
    if elem["type"] == "Literal":
        return Literal(elem["value"], elem["raw"], elem["regex"] if "regex" in elem else None)
    if elem["type"] == "MemberExpression":
        return MemberExpression(elem["computed"], parse_elem(elem["object"]), \
                                parse_elem(elem["property"]))
    if elem["type"] == "SpreadElement":
        return SpreadElement(parse_elem(elem["argument"]))
    if elem["type"] == "WhileStatement":
        return WhileStatement(parse_elem(elem["test"]), parse_elem(elem["body"]))

    raise TypeError("Unknown type")

def parse(program: dict) -> [Node]:
    nodes = []

    if program["type"] == "Program":
        for node in program["body"]:
            nodes.append(parse_elem(node))

    return nodes


def main():
    content: dict
    with open(program, "r") as f:
        content = json.load(f)

    vulns: dict
    with open(patterns, "r") as f:
        vulns = json.load(f)

    output = []

    nodes = parse(content)

    for vuln in vulns:
        analyser = Analyser()

        # create all the relevant elements
        for source in vuln["sources"]:
            analyser.addSource(Source(source))
        for sanitizer in vuln["sanitizers"]:
            analyser.addSanitizer(Sanitizer(sanitizer))
        for sink in vuln["sinks"]:
            analyser.addSink(Sink(sink))

        # traverse AST
        for node in nodes:
            node.accept(analyser)

        vuln_instances = analyser.vulns
        for instance in vuln_instances:
            dic = {"vulnerability": vuln["vulnerability"],
                   "source": [instance.source.path],
                   "sink": [instance.sink.path],
                   "sanitizer": [x.path for x in instance.sanitizer]
                   }
            output.append(dic)

    name_parts = program.rsplit('.', 1)
    out_file = name_parts[0] + '.output.json'

    with open(out_file, "w") as f:
        print(json.dumps(output))
        json.dump(output,f, indent=2)


if __name__ == '__main__':
    main()
