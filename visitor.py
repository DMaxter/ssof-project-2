import abc

class Visitor(abc.ABC):
    @abc.abstractmethod
    def visit_assignment_expression(self, node):
        pass

    @abc.abstractmethod
    def visit_binary_expression(self, node):
        pass

    @abc.abstractmethod
    def visit_block_statement(self, node):
        pass

    @abc.abstractmethod
    def visit_call_expression(self, node):
        pass

    @abc.abstractmethod
    def visit_expression_statement(self, node):
        pass

    @abc.abstractmethod
    def visit_identifier(self, node):
        pass

    @abc.abstractmethod
    def visit_if_statement(self, node):
        pass

    @abc.abstractmethod
    def visit_literal(self, node):
        pass

    @abc.abstractmethod
    def visit_member_expression(self, node):
        pass

    @abc.abstractmethod
    def visit_spread_element(self, node):
        pass

    @abc.abstractmethod
    def visit_while_statement(self, node):
        pass
